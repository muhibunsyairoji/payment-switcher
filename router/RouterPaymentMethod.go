package router

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/logger"
	"bitbucket.org/pergicom/payment-switcher/models"
	"bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/controllers"
	"bitbucket.org/pergicom/payment-switcher/repositories"
	"bitbucket.org/pergicom/payment-switcher/services"
	"bitbucket.org/pergicom/payment-switcher/utils"
	"github.com/gorilla/mux"
)

func (c *core) PaymentMethodInjector() controllers.PaymentMethodController {

	repository := &repositories.PaymentMethodRepository{Clients: c.Clients}
	service := &services.PaymentMethodService{IPaymentMethodRepository: repository}
	controller := controllers.PaymentMethodController{IPaymentMethodService: service}

	return controller
}

func (router *routerInjector) payment_method(w http.ResponseWriter, r *http.Request) {

	start := time.Now()

	rw := models.ResponseWrapper{
		TotalData:   0,
		Data:        []string{},
		Status:      false,
		Code:        cons.CONST_CODE_INTERNAL_SERVER_ERROR,
		Message:     "INTERNAL SERVER ERROR",
		ElapsedTime: "0ms",
	}

	defer func() {
		rw.ElapsedTime = time.Now().Sub(start).String()
		utils.ResponseWriter(w, rw)
	}()

	userBytes := []byte(r.Header.Get("User"))
	user := &models.User{}
	if err := json.Unmarshal(userBytes, &user); err != nil {
		logger.Errorf("json.Unmarshal user got = %v", r.Header.Get("User"), err)
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "invalid credential"
		return
	}

	vars := mux.Vars(r)
	switch strings.ToLower(vars["key"]) {
	case constants.API_KEY_QUERY:
		{
			if err := router.paymentMethodController.FindPaymentMethodByQueryController(r.Context(), r.Body, &rw); err != nil {
				break
			}
		}
	case constants.API_KEY_LIST:
		{
			if err := router.paymentMethodController.FindPaymentMethodController(r.Context(), r.Body, &rw); err != nil {
				break
			}
		}
	case constants.API_KEY_ADD:
		{
			if err := router.paymentMethodController.AddPaymentMethodController(r.Context(), r.Body, &rw, user); err != nil {
				break
			}
		}
	case constants.API_KEY_EDIT:
		{
			if err := router.paymentMethodController.EditPaymentMethodController(r.Context(), r.Body, &rw, user); err != nil {
				break
			}
		}
	case constants.API_KEY_DELETE:
		{
			if err := router.paymentMethodController.DeletePaymentMethodController(r.Context(), r.Body, &rw, user); err != nil {
				break
			}
		}
	/*case constants.API_KEY_ADD_BULK:
	{
		if err := router.paymentMethodController.AddBulkPaymentMethodController(r.Context(), r.Body, &rw); err != nil {
			break
		}
	}*/
	default:
		rw.Message = "Page not found"
		rw.Code = cons.CONST_CODE_NOT_FOUND
		return
	}

}
