// +build !test

package router

import (
	"net/http"
	"sync"

	"bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/controllers"
	"github.com/gorilla/mux"
)

type routerInjector struct {
	paymentMethodController   controllers.PaymentMethodController
	paymentScheduleController controllers.PaymentScheduleController
}

var (
	ri                 *routerInjector
	routerInjectorOnce sync.Once
)

type IMuxRouter interface {
	InitRouter() *mux.Router
}

type httpHandlerFunc func(http.ResponseWriter, *http.Request)

func (router *routerInjector) InitRouter() *mux.Router {

	router.paymentMethodController = Injector().PaymentMethodInjector()
	router.paymentScheduleController = Injector().PaymentScheduleInjector()

	r := mux.NewRouter()

	r.HandleFunc(constants.API_PAYMENT_METHOD, interceptor(router.payment_method)).Methods("POST")
	r.HandleFunc(constants.API_PAYMENT_SCHEDULE, interceptor(router.payment_schedule)).Methods("POST")

	return r
}

func interceptor(next httpHandlerFunc) httpHandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		next(res, req)
	}
}

func MuxRouter() IMuxRouter {
	if ri == nil {
		routerInjectorOnce.Do(func() {
			ri = &routerInjector{}
		})
	}
	return ri
}
