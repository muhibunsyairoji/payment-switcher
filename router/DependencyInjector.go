package router

import (
	"fmt"
	"log"
	"sync"

	"bitbucket.org/pergicom/payment-switcher/config"
	"bitbucket.org/pergicom/payment-switcher/controllers"
	"bitbucket.org/pergicom/payment-switcher/utils"
)

var (
	C            *core
	injectorOnce sync.Once
)

type core struct {
	Clients *config.Clients
}

type IDependencyInjector interface {
	PaymentMethodInjector() controllers.PaymentMethodController
	PaymentScheduleInjector() controllers.PaymentScheduleController
}

func Injector() IDependencyInjector {
	if C == nil {
		injectorOnce.Do(func() {
			C = &core{}
			clients, err := config.InitServer()
			if err != nil {
				log.Fatalf("INTERNAL SERVER ERROR : %v", err)
			}
			C.Clients = clients
			fmt.Println("Migrating Tables, Please wait...")
			if err := utils.AutoMigrates(C.Clients.DB); err != nil {
				log.Fatalf("INTERNAL SERVER ERROR : %v", err)
			}
			fmt.Println("Migrating Tables, Done !")
			/*fmt.Println("Mapping Elastic Indexes, Please wait...")
			if err := utils.ElasticIndexMapping(c.Clients.Elastic, c.Clients.Properties); err != nil {
				log.Fatalf("INTERNAL SERVER ERROR : %v", err)
			}
			fmt.Println("Mapping Elastic Indexes, Done !")*/
		})
	}
	return C
}
