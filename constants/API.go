package constants

const (
	API_PAYMENT_METHOD         = "/payment-switcher/payment-method/{key}"
	API_PAYMENT_METHOD_GATEWAY = "/payment-switcher/payment-method-gateway/{key}"
	API_PAYMENT_METHOD_PRODUCT = "/payment-switcher/payment-method-product/{key}"
	API_PAYMENT_SCHEDULE       = "/payment-switcher/payment-schedule/{key}"
	API_PRODUCT_TYPE           = "/payment-switcher/product-type/{key}"
	API_KEY_LIST               = "list"
	API_KEY_ADD                = "add"
	API_KEY_EDIT               = "edit"
	API_KEY_DELETE             = "delete"
	API_KEY_QUERY              = "query"
)
