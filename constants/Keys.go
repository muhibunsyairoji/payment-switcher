package constants

const (
	CONST_ENV_FILE           = "app.env"
	CONST_KEY_REDIS_URL      = "REDIS_URL"
	CONST_KEY_REDIS_PASSWORD = "REDIS_PASSWORD"
	CONST_KEY_CONFIG         = "PAYMENT_SWITCHER_CONFIG"
	CONST_KEY_PORT           = "PORT"
	CONST_LOG_NAME           = "payment-switcher-"
	CONST_DB_DIALECT         = "mysql"
)
