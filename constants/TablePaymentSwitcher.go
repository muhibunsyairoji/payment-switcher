package constants

const (
	TBL_PAYMENT_METHOD         = "payment_method"
	TBL_PAYMENT_GATEWAY        = "payment_gateway"
	TBL_PAYMENT_METHOD_LIST    = "payment_method_list"
	TBL_PAYMENT_SCHEDULE       = "payment_schedule"
	TBL_PAYMENT_PRODUCT_METHOD = "payment_product_method"
	TBL_PRODUCT_TYPE           = "product_type"
	TBL_PRODUCT                = "product"
)
