package controllers

import (
	"context"
	"errors"
	"io"
	"strings"

	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/interfaces"
	"bitbucket.org/pergicom/payment-switcher/models"
	"bitbucket.org/pergicom/payment-switcher/utils"
)

type PaymentScheduleController struct {
	interfaces.IPaymentScheduleService
}

func (c *PaymentScheduleController) FindPaymentScheduleByQueryController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper) error {

	var param models.PaymentSchedule
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to find paymentSchedule : invalid parameters"
		return err
	}

	offset := 0
	size := 1000

	query := ""
	if param.Query != nil && *param.Query != "" {
		query = utils.SanitizeQuery(*param.Query)
	}

	if param.Page != nil && param.Size != nil {
		offset = (*param.Page - 1) * (*param.Size)
		size = *param.Size
	} else if param.Page != nil && param.Size == nil {
		offset = (*param.Page - 1) * size
	} else if param.Page == nil && param.Size != nil {
		size = *param.Size
	}

	parameter := &models.PaymentSchedule{}

	parameter.Query = &query
	parameter.Descending = param.Descending

	defaultOrder := "payment_schedule_id"

	if param.OrderBy != nil {
		parameter.OrderBy = utils.ToSnakeCase(param.OrderBy)
	} else {
		parameter.OrderBy = &defaultOrder
	}

	if err := c.FindPaymentScheduleByQueryService(ctx, parameter, offset, size, rw); err != nil {
		return err
	}

	return nil
}

func (c *PaymentScheduleController) FindPaymentScheduleController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper) error {

	var param models.PaymentSchedule
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to find paymentSchedule : invalid parameters"
		return err
	}

	offset := 0
	size := 1000

	if param.Page != nil && param.Size != nil {
		offset = (*param.Page - 1) * (*param.Size)
		size = *param.Size
	} else if param.Page != nil && param.Size == nil {
		offset = (*param.Page - 1) * size
	} else if param.Page == nil && param.Size != nil {
		size = *param.Size
	}

	defaultOrder := "payment_schedule_id"

	parameter := &models.PaymentSchedule{
		PaymentScheduleID:      param.PaymentScheduleID,
		PaymentProductMethodID: param.PaymentProductMethodID,
		RecordProperties:       models.RecordProperties{},
	}

	parameter.Descending = param.Descending

	if param.OrderBy != nil {
		parameter.OrderBy = utils.ToSnakeCase(param.OrderBy)
	} else {
		parameter.OrderBy = &defaultOrder
	}

	if err := c.FindPaymentScheduleService(ctx, parameter, offset, size, rw); err != nil {
		return err
	}

	return nil
}

func (c *PaymentScheduleController) AddPaymentScheduleController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper, user *models.User) error {

	var param models.PaymentSchedule
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to add paymentSchedule : invalid parameters"
		return err
	}

	if param.DateStartedAt == nil || strings.TrimSpace(*param.DateStartedAt) == "" {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to add paymentSchedule : DateStartedAt is required"
		return errors.New("parameter error")
	}

	// if param.Code != nil && len(strings.TrimSpace(*param.Code)) != 2 {
	// 	rw.Code = cons.CONST_CODE_BAD_REQUEST
	// 	rw.Message = "Unable to add paymentSchedule : paymentScheduleCode must be 2 character"
	// 	return errors.New("parameter error")
	// }

	if param.DateEndedAt == nil || strings.TrimSpace(*param.DateEndedAt) == "" {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to add paymentSchedule : DateEndedAt is required"
		return errors.New("parameter error")
	}

	parameter := &models.PaymentSchedule{
		PaymentScheduleID:      nil,
		PaymentProductMethodID: param.PaymentProductMethodID,
		ScheduleType:           param.ScheduleType,
		ScheduleStatus:         param.ScheduleStatus,
		DateStartedAt:          param.DateStartedAt,
		DateEndedAt:            param.DateEndedAt,
		TimeStartedAt:          param.TimeStartedAt,
		TimeEndedAt:            param.TimeEndedAt,
		RecordProperties:       models.RecordProperties{},
	}

	if err := c.AddPaymentScheduleService(ctx, parameter, rw, user); err != nil {
		return err
	}
	return nil
}

func (c *PaymentScheduleController) EditPaymentScheduleController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper, user *models.User) error {

	var param models.PaymentSchedule
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentSchedule : invalid parameters"
		return err
	}

	if param.PaymentScheduleID == nil || *param.PaymentScheduleID <= int64(0) {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentSchedule : paymentSchedule id is required"
		return errors.New("parameter error")
	}

	if (param.DateStartedAt == nil || strings.TrimSpace(*param.DateStartedAt) == "") && (param.DateEndedAt == nil || strings.TrimSpace(*param.DateEndedAt) == "") {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentSchedule : one of the following (paymentSchedule DateStartedAt | paymentSchedule DateEndedAt) is required"
		return errors.New("parameter error")
	}

	if param.ScheduleStatus != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentSchedule : ScheduleStatus is required"
		return errors.New("parameter error")
	}

	parameter := &models.PaymentSchedule{
		PaymentScheduleID:      param.PaymentScheduleID,
		PaymentProductMethodID: param.PaymentProductMethodID,
		ScheduleType:           param.ScheduleType,
		ScheduleStatus:         param.ScheduleStatus,
		DateStartedAt:          param.DateStartedAt,
		DateEndedAt:            param.DateEndedAt,
		TimeStartedAt:          param.TimeStartedAt,
		TimeEndedAt:            param.TimeEndedAt,
		RecordProperties:       models.RecordProperties{},
	}

	if err := c.EditPaymentScheduleService(ctx, parameter, rw, user); err != nil {
		return err
	}

	return nil
}

func (c *PaymentScheduleController) DeletePaymentScheduleController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper, user *models.User) error {

	var param models.PaymentSchedule
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to delete master paymentSchedule : invalid parameters"
		return errors.New("parameter error")
	}

	if param.PaymentScheduleID == nil || *param.PaymentScheduleID == int64(0) {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to delete master paymentSchedule : paymentSchedule id is required"
		return errors.New("parameter error")
	}

	parameter := &models.PaymentSchedule{
		PaymentScheduleID: param.PaymentScheduleID,
		RecordProperties:  models.RecordProperties{},
	}

	if err := c.DeletePaymentScheduleService(ctx, parameter, rw, user); err != nil {
		return err
	}

	return nil
}

/*func (c *PaymentScheduleController) AddBulkPaymentScheduleController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper) error {

	var param []models.PaymentSchedule
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		return err
	}

	if err := c.AddBulkPaymentScheduleService(ctx context.Context, param, rw); err != nil {
		return err
	}
	return nil
}*/
