package controllers

import (
	"context"
	"errors"
	"io"
	"strings"

	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/interfaces"
	"bitbucket.org/pergicom/payment-switcher/models"
	"bitbucket.org/pergicom/payment-switcher/utils"
)

type PaymentMethodController struct {
	interfaces.IPaymentMethodService
}

func (c *PaymentMethodController) FindPaymentMethodByQueryController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper) error {

	var param models.PaymentMethod
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to find paymentMethod : invalid parameters"
		return err
	}

	offset := 0
	size := 1000

	query := ""
	if param.Query != nil && *param.Query != "" {
		query = utils.SanitizeQuery(*param.Query)
	}

	if param.Page != nil && param.Size != nil {
		offset = (*param.Page - 1) * (*param.Size)
		size = *param.Size
	} else if param.Page != nil && param.Size == nil {
		offset = (*param.Page - 1) * size
	} else if param.Page == nil && param.Size != nil {
		size = *param.Size
	}

	parameter := &models.PaymentMethod{}

	parameter.Query = &query
	parameter.Descending = param.Descending

	defaultOrder := "payment_method_id"

	if param.OrderBy != nil {
		parameter.OrderBy = utils.ToSnakeCase(param.OrderBy)
	} else {
		parameter.OrderBy = &defaultOrder
	}

	if err := c.FindPaymentMethodByQueryService(ctx, parameter, offset, size, rw); err != nil {
		return err
	}

	return nil
}

func (c *PaymentMethodController) FindPaymentMethodController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper) error {

	var param models.PaymentMethod
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to find paymentMethod : invalid parameters"
		return err
	}

	offset := 0
	size := 1000

	if param.Page != nil && param.Size != nil {
		offset = (*param.Page - 1) * (*param.Size)
		size = *param.Size
	} else if param.Page != nil && param.Size == nil {
		offset = (*param.Page - 1) * size
	} else if param.Page == nil && param.Size != nil {
		size = *param.Size
	}

	paymentMethodCode := utils.TrimUpperString(param.Code)
	paymentMethodName := utils.TrimTitleString(param.Name)
	paymentMethodType := utils.TrimTitleString(param.Type)
	paymentGatewayID := param.PaymentGatewayID
	defaultOrder := "payment_method_id"

	parameter := &models.PaymentMethod{
		PaymentMethodID:  param.PaymentMethodID,
		Code:             paymentMethodCode,
		Name:             paymentMethodName,
		Type:             paymentMethodType,
		PaymentGatewayID: paymentGatewayID,
		RecordProperties: models.RecordProperties{},
	}

	parameter.Descending = param.Descending

	if param.OrderBy != nil {
		parameter.OrderBy = utils.ToSnakeCase(param.OrderBy)
	} else {
		parameter.OrderBy = &defaultOrder
	}

	if err := c.FindPaymentMethodService(ctx, parameter, offset, size, rw); err != nil {
		return err
	}

	return nil
}

func (c *PaymentMethodController) AddPaymentMethodController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper, user *models.User) error {

	var param models.PaymentMethod
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to add paymentMethod : invalid parameters"
		return err
	}

	if param.Code == nil || strings.TrimSpace(*param.Code) == "" {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to add paymentMethod : paymentMethodCode is required"
		return errors.New("parameter error")
	}

	// if param.Code != nil && len(strings.TrimSpace(*param.Code)) != 2 {
	// 	rw.Code = cons.CONST_CODE_BAD_REQUEST
	// 	rw.Message = "Unable to add paymentMethod : paymentMethodCode must be 2 character"
	// 	return errors.New("parameter error")
	// }

	if param.Name == nil || strings.TrimSpace(*param.Name) == "" {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to add paymentMethod : paymentMethodName is required"
		return errors.New("parameter error")
	}

	paymentMethodCode := utils.TrimUpperString(param.Code)
	paymentMethodName := utils.TrimTitleString(param.Name)

	parameter := &models.PaymentMethod{
		PaymentMethodID:  nil,
		Code:             paymentMethodCode,
		Name:             paymentMethodName,
		RecordProperties: models.RecordProperties{},
	}

	if err := c.AddPaymentMethodService(ctx, parameter, rw, user); err != nil {
		return err
	}
	return nil
}

func (c *PaymentMethodController) EditPaymentMethodController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper, user *models.User) error {

	var param models.PaymentMethod
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentMethod : invalid parameters"
		return err
	}

	if param.PaymentMethodID == nil || *param.PaymentMethodID <= int64(0) {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentMethod : paymentMethod id is required"
		return errors.New("parameter error")
	}

	if (param.Code == nil || strings.TrimSpace(*param.Code) == "") && (param.Name == nil || strings.TrimSpace(*param.Name) == "") {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentMethod : one of the following (paymentMethod Code | paymentMethod Name) is required"
		return errors.New("parameter error")
	}

	if param.Code != nil && len(strings.TrimSpace(*param.Code)) != 2 {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to edit master paymentMethod : paymentMethodCode must be 2 character"
		return errors.New("parameter error")
	}

	paymentMethodCode := utils.TrimUpperString(param.Code)
	paymentMethodName := utils.TrimTitleString(param.Name)

	parameter := &models.PaymentMethod{
		PaymentMethodID:  param.PaymentMethodID,
		Code:             paymentMethodCode,
		Name:             paymentMethodName,
		RecordProperties: models.RecordProperties{},
	}

	if err := c.EditPaymentMethodService(ctx, parameter, rw, user); err != nil {
		return err
	}

	return nil
}

func (c *PaymentMethodController) DeletePaymentMethodController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper, user *models.User) error {

	var param models.PaymentMethod
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to delete master paymentMethod : invalid parameters"
		return errors.New("parameter error")
	}

	if param.PaymentMethodID == nil || *param.PaymentMethodID == int64(0) {
		rw.Code = cons.CONST_CODE_BAD_REQUEST
		rw.Message = "Unable to delete master paymentMethod : paymentMethod id is required"
		return errors.New("parameter error")
	}

	parameter := &models.PaymentMethod{
		PaymentMethodID:  param.PaymentMethodID,
		RecordProperties: models.RecordProperties{},
	}

	if err := c.DeletePaymentMethodService(ctx, parameter, rw, user); err != nil {
		return err
	}

	return nil
}

/*func (c *PaymentMethodController) AddBulkPaymentMethodController(ctx context.Context, requestBody io.ReadCloser, rw *models.ResponseWrapper) error {

	var param []models.PaymentMethod
	if err := utils.ParamDecoder(requestBody, &param); err != nil {
		return err
	}

	if err := c.AddBulkPaymentMethodService(ctx context.Context, param, rw); err != nil {
		return err
	}
	return nil
}*/
