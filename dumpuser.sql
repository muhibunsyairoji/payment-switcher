-- MySQL dump 10.13  Distrib 8.0.17, for Linux (x86_64)
--
-- Host: 35.198.223.118    Database: dev_fountaindb
-- ------------------------------------------------------
-- Server version	5.7.14-google

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_admin`
--

DROP TABLE IF EXISTS `user_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_admin` (
  `admin_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_role_id` int(11) DEFAULT NULL,
  `admin_username` varchar(50) NOT NULL,
  `admin_password` varchar(100) NOT NULL,
  `admin_fullname` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_lastlogin` datetime DEFAULT NULL,
  `admin_allowlogin` int(1) NOT NULL DEFAULT '0',
  `date_in` datetime NOT NULL,
  `user_in` varchar(50) NOT NULL,
  `date_up` datetime DEFAULT NULL,
  `user_up` varchar(50) DEFAULT NULL,
  `status_record` char(1) NOT NULL,
  PRIMARY KEY (`admin_id`),
  KEY `admin_role_id` (`admin_role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_admin`
--

LOCK TABLES `user_admin` WRITE;
/*!40000 ALTER TABLE `user_admin` DISABLE KEYS */;
INSERT INTO `user_admin` VALUES (1,1,'fatta@pergi.com','$2y$10$cgaA.Pc7jx/L9G8iEZSrcuP9Vh0oxbD2pMnaPMe.9m52in8Ibgb2a','Fatta','fatta@pergi.com',NULL,1,'2019-09-27 07:24:44','Sys',NULL,NULL,'A'),(2,1,'ibnu@pergi.com','$2y$10$T/MINrRoisPWppftkX99LurBnTfJbvCP9hQceqaxesxZ2zINlmnsC','Ibnu','ibnu@pergi.com',NULL,1,'2019-09-27 07:24:44','Fatta',NULL,NULL,'A');
/*!40000 ALTER TABLE `user_admin` ENABLE KEYS */;
UNLOCK TABLES;
