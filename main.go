// +build !test

package main

import (
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/router"
	"github.com/gorilla/handlers"
	"github.com/joho/godotenv"
)

func main() {

	if err := godotenv.Load(constants.CONST_ENV_FILE); err != nil {
		log.Fatalf("Fatal Error : %v", err.Error())
	}

	r := router.MuxRouter().InitRouter()
	port := *router.C.Clients.Properties.PortApp
	fmt.Println("---------------------------------------")
	fmt.Println("payment-switcher app run on port " + port)
	fmt.Println("---------------------------------------")
	http.Handle("/", r)
	originsOk := handlers.AllowedOrigins([]string{"*"})
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	err := http.ListenAndServe(":"+port, handlers.CORS(originsOk, headersOk, methodsOk)(r))

	if err != nil {
		log.Fatalf("Fatal Error : %v", err.Error())
	}
}
