package interfaces

import (
	"context"

	"bitbucket.org/pergicom/payment-switcher/models"
)

type IPaymentScheduleService interface {
	FindPaymentScheduleByQueryService(ctx context.Context, param *models.PaymentSchedule, offset int, size int, rw *models.ResponseWrapper) error
	FindPaymentScheduleService(ctx context.Context, param *models.PaymentSchedule, offset int, size int, rw *models.ResponseWrapper) error
	AddPaymentScheduleService(ctx context.Context, param *models.PaymentSchedule, rw *models.ResponseWrapper, user *models.User) error
	EditPaymentScheduleService(ctx context.Context, param *models.PaymentSchedule, rw *models.ResponseWrapper, user *models.User) error
	DeletePaymentScheduleService(ctx context.Context, param *models.PaymentSchedule, rw *models.ResponseWrapper, user *models.User) error
	//AddBulkPaymentScheduleService(ctx context.Context, param []models.PaymentSchedule, rw *models.ResponseWrapper) error
}

type IPaymentScheduleRepository interface {
	FindPaymentScheduleByQueryRepository(ctx context.Context, param *models.PaymentSchedule, offset int, size int) ([]models.PaymentSchedule, int, error)
	FindPaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, offset int, size int) ([]models.PaymentSchedule, int, error)
	AddPaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, user *models.User) ([]models.PaymentSchedule, error)
	EditPaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, user *models.User) ([]models.PaymentSchedule, error)
	DeletePaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, user *models.User) ([]models.PaymentSchedule, error)
	//AddBulkPaymentScheduleRepository(ctx, param []models.PaymentSchedule) ([]models.PaymentSchedule, error)
}
