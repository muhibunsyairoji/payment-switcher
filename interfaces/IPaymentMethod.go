package interfaces

import (
	"context"

	"bitbucket.org/pergicom/payment-switcher/models"
)

type IPaymentMethodService interface {
	FindPaymentMethodByQueryService(ctx context.Context, param *models.PaymentMethod, offset int, size int, rw *models.ResponseWrapper) error
	FindPaymentMethodService(ctx context.Context, param *models.PaymentMethod, offset int, size int, rw *models.ResponseWrapper) error
	AddPaymentMethodService(ctx context.Context, param *models.PaymentMethod, rw *models.ResponseWrapper, user *models.User) error
	EditPaymentMethodService(ctx context.Context, param *models.PaymentMethod, rw *models.ResponseWrapper, user *models.User) error
	DeletePaymentMethodService(ctx context.Context, param *models.PaymentMethod, rw *models.ResponseWrapper, user *models.User) error
	//AddBulkPaymentMethodService(ctx context.Context, param []models.PaymentMethod, rw *models.ResponseWrapper) error
}

type IPaymentMethodRepository interface {
	FindPaymentMethodByQueryRepository(ctx context.Context, param *models.PaymentMethod, offset int, size int) ([]models.PaymentMethod, int, error)
	FindPaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, offset int, size int) ([]models.PaymentMethod, int, error)
	AddPaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, user *models.User) ([]models.PaymentMethod, error)
	EditPaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, user *models.User) ([]models.PaymentMethod, error)
	DeletePaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, user *models.User) ([]models.PaymentMethod, error)
	//AddBulkPaymentMethodRepository(ctx, param []models.PaymentMethod) ([]models.PaymentMethod, error)
}
