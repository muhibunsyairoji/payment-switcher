package utils

import (
	"fmt"

	"bitbucket.org/pergicom/payment-switcher/constants"
	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/models"
	"github.com/jinzhu/gorm"
)

func AutoMigrates(db *gorm.DB) error {

	err := db.DB().Ping()
	if err != nil {
		panic(err)
	}

	if !db.HasTable(&models.PaymentGateway{}) {
		if err := db.Set("gorm:table_options",
			"ENGINE=InnoDB CHARSET=utf8 auto_increment=1").AutoMigrate(&models.PaymentGateway{}).Error; err != nil {
		}
		if err := db.Exec("INSERT INTO " + cons.TBL_PAYMENT_GATEWAY + " (payment_gateway_id,code,name, created_at, created_by, status_record) VALUES " +
			constants.QUERY_VALUES_PAYMENT_GATEWAY).Error; err != nil {
		}
	}

	if !db.HasTable(&models.PaymentMethod{}) {
		if err := db.Set("gorm:table_options",
			"ENGINE=InnoDB CHARSET=utf8 auto_increment=1").AutoMigrate(&models.PaymentMethod{}).Error; err != nil {
		}
		if err := db.Exec("INSERT INTO " + cons.TBL_PAYMENT_METHOD + " (payment_method_id, payment_gateway_id, type,method_code, code, name, short_name, is_default, is_active, created_at, created_by, status_record) VALUES " +
			constants.QUERY_VALUES_PAYMENT_METHOD).Error; err != nil {
			fmt.Println(err)
		}
	}

	if !db.HasTable(&models.PaymentSchedule{}) {
		if err := db.Set("gorm:table_options",
			"ENGINE=InnoDB CHARSET=utf8 auto_increment=1").AutoMigrate(&models.PaymentSchedule{}).Error; err != nil {
		}
	}

	if !db.HasTable(&models.PaymentProductMethod{}) {
		if err := db.Set("gorm:table_options",
			"ENGINE=InnoDB CHARSET=utf8 auto_increment=1").AutoMigrate(&models.PaymentProductMethod{}).Error; err != nil {
		}

		if err := db.Exec("INSERT INTO " + cons.TBL_PAYMENT_PRODUCT_METHOD + " (payment_method_id, product_type, service_charge, service_charge_type, minimum_payment, sort, is_active, created_at, created_by, status_record) SELECT payment_method_id, 'tour', 0, 'nominal', 0, payment_method_id, 1, NOW(), 'SYSTEM', 'A' FROM " + cons.TBL_PAYMENT_METHOD).Error; err != nil {
			fmt.Println(err)
		}
	}

	return nil
}
