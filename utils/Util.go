package utils

import (
	"fmt"
	"github.com/liamylian/jsontime"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var json = jsontime.ConfigWithCustomTimeFormat

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func PrintStruct(param interface{}) {
	out, err := json.MarshalIndent(&param, "", " ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(out))
}

func ResponseWriter(w http.ResponseWriter, i interface{}) {
	buf, err := json.Marshal(i)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "applciation/json")
	_, _ = w.Write(buf)
}

func ParamDecoder(responseBody io.ReadCloser, param interface{}) error {
	if err := json.NewDecoder(responseBody).Decode(&param); err != nil {
		return err
	}
	return nil
}

func SanitizeQuery(text string) string {
	return strings.NewReplacer("'", "''").Replace(text)
}

func TrimUpperString(s *string) *string {
	if s != nil && strings.TrimSpace(*s) != "" {
		str := strings.ToUpper(strings.TrimSpace(*s))
		return &str
	}
	return nil
}

func TrimTitleString(s *string) *string {
	if s != nil && strings.TrimSpace(*s) != "" {
		str := strings.Title(strings.TrimSpace(*s))
		return &str
	}
	return nil
}

func ToSnakeCase(s *string) *string {
	if s != nil && strings.TrimSpace(*s) != "" {
		str := matchFirstCap.ReplaceAllString(*s, "${1}_${2}")
		str = matchAllCap.ReplaceAllString(str, "${1}_${2}")
		snakeCase := strings.ToLower(str)
		return &snakeCase
	}
	return nil
}

func GenerateTourCode() string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"0123456789")
	length := 8
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

func GenerateSlug(title string) *string {

	reg, err := regexp.Compile(`[^a-zA-Z0-9 ]`)
	if err != nil {
		log.Fatal(err)
	}

	processedString := reg.ReplaceAllString(title, "")

	splitTitle := strings.Split(processedString, " ")
	randNumb := 1 + rand.Intn(999-1)
	var slug string
	for i, v := range splitTitle {
		if strings.TrimSpace(v) != "" {
			if i == len(splitTitle)-1 {
				slug += strings.ToLower(v) +"-"+strconv.Itoa(randNumb)
			} else {
				slug += strings.ToLower(strings.TrimSpace(v))+ "-"
			}
		}
	}

	return &slug
}

func GenerateUUID() (*string, error) {
	uid, err := exec.Command("uuidgen").Output()
	if err != nil {
		return nil, err
	}

	code := strings.TrimRight(string(uid), "\n")

	return &code, nil
}

/*func ElasticExecInsert(client *elastic.Client, eIndex string, eType string, id string, doc interface{}) (err error) {

	ctx := context.Background()
	_, err = client.Index().
		Index(eIndex).
		Type(eType).
		Id(id).
		Refresh("true").
		BodyJson(doc).
		Do(ctx)
	if err != nil {
		return err
	}

	_, err = client.Flush().Index(eIndex).Do(ctx)
	if err != nil {
		panic(err)
	}

	return nil
}

func ElasticExecQuery(client *elastic.Client, searchSource *elastic.SearchSource, eIndex string) (sr *elastic.SearchResult, err error) {

	ctx := context.Background()
	exists, err := client.IndexExists(eIndex).Do(ctx)
	if err != nil {
		return nil, err
	}

	if exists {

		searchResult, err := client.Search().
			Index(eIndex).
			SearchSource(searchSource).
			Do(ctx)
		if err != nil {
			return sr, err
		}

		return searchResult, nil
	}

	return nil, err

}

func ElasticExecUpdateByScript(client *elastic.Client, eIndex string, query *elastic.BoolQuery, script *elastic.Script) (err error) {

	ctx := context.Background()
	_, err = client.UpdateByQuery().
		Index(eIndex).
		Query(query).
		Script(script).
		Refresh("true").
		Do(ctx)

	if err != nil {
		return err
	}

	_, err = client.Flush().Index(eIndex).Do(ctx)
	if err != nil {
		panic(err)
	}

	return nil
}

func ElasticExecUpsert(client *elastic.Client, eIndex string, eType string, id string, doc interface{}) (err error) {

	ctx := context.Background()
	_, err = client.Update().
		Index(eIndex).
		Type(eType).
		Id(id).
		DocAsUpsert(true).
		Doc(doc).
		Refresh("true").
		Do(ctx)
	if err != nil {
		return err
	}

	return nil
}*/
