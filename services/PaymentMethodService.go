package services

import (
	"context"

	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/models"
	"bitbucket.org/pergicom/payment-switcher/interfaces"
)

type PaymentMethodService struct {
	interfaces.IPaymentMethodRepository
}

func (service *PaymentMethodService) FindPaymentMethodByQueryService(ctx context.Context, param *models.PaymentMethod, offset int, size int, rw *models.ResponseWrapper) error {

	result, totalData, err := service.FindPaymentMethodByQueryRepository(ctx, param, offset, size)
	if err != nil {
		return err
	}

	if len(result) > 0 {
		hasMorePage := totalData > size
		rw.Status = true
		rw.Code = cons.CONST_CODE_SUCCESS
		rw.Data = result
		rw.TotalData = totalData
		rw.HasMorePage = &hasMorePage
		rw.Message = "Success"
	} else {
		rw.Code = cons.CONST_CODE_NOT_FOUND
		rw.Message = "Record not found"
		rw.Status = false
	}

	return nil

}

func (service *PaymentMethodService) FindPaymentMethodService(ctx context.Context, param *models.PaymentMethod, offset int, size int, rw *models.ResponseWrapper) error {

	result, totalData, err := service.FindPaymentMethodRepository(ctx, param, offset, size)
	if err != nil {
		return err
	}

	if len(result) > 0 {
		hasMorePage := totalData > size
		rw.Data = result
		rw.TotalData = totalData
		rw.Message = "Success"
		rw.HasMorePage = &hasMorePage
	} else {
		rw.Message = "Record not found"
	}

	rw.Code = cons.CONST_CODE_SUCCESS
	rw.Message = "Success"
	rw.Data = result
	rw.Status = true

	return nil
}

func (service *PaymentMethodService) AddPaymentMethodService(ctx context.Context, param *models.PaymentMethod, rw *models.ResponseWrapper, user *models.User) error {

	result, err := service.AddPaymentMethodRepository(ctx, param, user)
	if err != nil {
		switch err.(type) {
		case *models.CustomError:
			rw.Status = false
			rw.Code = cons.CONST_CODE_BAD_REQUEST
			rw.Message = err.Error()
			return err
		default:
			return err
		}
	}

	rw.Data = result
	rw.TotalData = 1
	rw.Code = cons.CONST_CODE_SUCCESS
	rw.Status = true
	rw.Message = "Success"

	return nil

}

func (service *PaymentMethodService) EditPaymentMethodService(ctx context.Context, param *models.PaymentMethod, rw *models.ResponseWrapper, user *models.User) error {

	result, err := service.EditPaymentMethodRepository(ctx, param, user)
	if err != nil {
		switch err.(type) {
		case *models.CustomError:
			rw.Status = false
			rw.Code = cons.CONST_CODE_BAD_REQUEST
			rw.Message = err.Error()
			return err
		default:
			return err
		}
	}

	rw.Data = result
	rw.TotalData = 1
	rw.Code = cons.CONST_CODE_SUCCESS
	rw.Status = true
	rw.Message = "Success"

	return nil

}

func (service *PaymentMethodService) DeletePaymentMethodService(ctx context.Context, param *models.PaymentMethod, rw *models.ResponseWrapper, user *models.User) error {

	result, err := service.DeletePaymentMethodRepository(ctx, param, user)
	if err != nil {
		switch err.(type) {
		case *models.CustomError:
			rw.Status = false
			rw.Code = cons.CONST_CODE_BAD_REQUEST
			rw.Message = err.Error()
			return err
		default:
			return err
		}
	}

	rw.Data = result
	rw.TotalData = 1
	rw.Code = cons.CONST_CODE_SUCCESS
	rw.Status = true
	rw.Message = "Success"

	return nil

}
