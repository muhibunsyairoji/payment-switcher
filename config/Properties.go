package config

type Properties struct {
	LogPath              *string `json:"logPath"`
	PortApp              *string `json:"portApp"`
	DbURL                *string `json:"dbURL"`
	ElasticURL           *string `json:"elasticURL"`
	BucketCredentialPath *string `json:"bucketCredentialPath"`
	BucketProjectID      *string `json:"bucketProjectID"`
	BucketName           *string `json:"bucketName"`
	ImageURL             *string `json:"imageURL"`
	ImagePath            *string `json:"imagePath"`
	TimeoutLimitSecond   *int    `json:"timeoutLimitSeconds"`
	PaymentURI           *string `json:"paymentURI"`
	DaysBeforeStart      *int    `json:"daysBeforeStart"`
	MaxOpenDBConnections *int    `json:"maxOpenDBConnections"`
	MaxIdleDBConnections *int    `json:"maxIdleDBConnections"`
	B2bURL               *string `json:"b2bURL"`
	MidtransURL          *string `json:"midtransURL"`
	NicepayURL           *string `json:"nicepayURL"`
}
