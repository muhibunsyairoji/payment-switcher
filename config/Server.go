// +build !test

package config

import (
	"bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/logger"
	"github.com/go-redis/redis"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/liamylian/jsontime"
	"github.com/olivere/elastic"
	"os"
)

var json = jsontime.ConfigWithCustomTimeFormat

type Clients struct {
	DB *gorm.DB
	Elastic *elastic.Client
	Redis *redis.Client
	Properties *Properties
}

func InitServer() (*Clients, error) {

	c := &Clients{}

	c.GetClientRedis(constants.CONST_KEY_REDIS_URL, constants.CONST_KEY_REDIS_PASSWORD)

	if err := c.GetProperties(constants.CONST_KEY_CONFIG); err != nil {
		return nil, err
	}

	if err := c.GetClientDB(constants.CONST_DB_DIALECT, *c.Properties.DbURL); err != nil {
		return nil,  err
	}
	/*if err := c.GetClientElastic(*c.Properties.ElasticURL); err != nil {
		return nil,  err
	}*/

	return c, nil
}

func (c *Clients) GetClientRedis(keyURL string, keyPassword string) {


	client := redis.NewClient(&redis.Options{
		Addr:     os.Getenv(keyURL),
		Password: os.Getenv(keyPassword),
		DB:       0,
	})

	c.Redis = client
}

func (c *Clients) GetProperties(keyRedisProperties string) error {

	c.Properties = &Properties{}
	props, err := c.Redis.Get(os.Getenv(keyRedisProperties)).Bytes()
	if err != nil {
		return err
	}

	if err := json.Unmarshal(props, &c.Properties); err != nil {
		return err
	}

	if err := logger.InitLog(*c.Properties.LogPath, constants.CONST_LOG_NAME); err != nil {
		return err
	}

	return  nil
}

func (c *Clients) GetClientDB(dialect string, uri string) error {
	db, err := gorm.Open(dialect, uri)
	if err != nil {
		return err
	}

	db.DB().SetMaxIdleConns(*c.Properties.MaxIdleDBConnections)
	db.DB().SetMaxOpenConns(*c.Properties.MaxOpenDBConnections)

	c.DB = db
	return nil
}

func (c *Clients) GetClientElastic(uri string) error {

	elasticClient, err := elastic.NewClient(elastic.SetURL(uri))
	if err != nil {
		return err
	}

	c.Elastic = elasticClient

	return nil
}



