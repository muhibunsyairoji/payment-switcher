// +build !test

package logger

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

var (
	logger = logrus.New()
	filePath string
	fileName string
)

type Fields map[string]interface{}

func InitLog(path string, name string) error {
	filePath = path
	fileName = name

	if _, err := os.Stat(path); os.IsNotExist(err) {
		if err := os.MkdirAll(path, os.ModePerm); err != nil {
			return err
		}
	}

	return nil

}

func SetOutput() error {
	logger.SetFormatter(&logrus.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})

	logger.SetFormatter(&logrus.JSONFormatter{})
	f, err := os.OpenFile(filePath+fileName+time.Now().Format("2006-01-02")+".log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	logger.Out = f

	return nil
}

func WithField(key string, value interface{}) *logrus.Entry {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	return logger.WithField(key, value)
}

func Info(args ...interface{}) {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	logger.Info(args...)
}

func WithFields(fields logrus.Fields) *logrus.Entry {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	return logger.WithFields(fields)
}

func WithError(err error) *logrus.Entry {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	return logger.WithField("error", err)
}

func Infof(s string, i ...interface{}) {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	logger.Infof(s, i)
}

func Warnf(s string, i ...interface{}) {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	logger.Warnf(s, i)
}

func Errorf(s string, i ...interface{}) {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	logger.Errorf(s, i)
}

func Fatalf(s string, i ...interface{}) {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	logger.Fatalf(s, i)
}

func Panicf(s string, i ...interface{}) {
	if err := SetOutput(); err != nil {
		fmt.Println("Logger error : "+err.Error())
	}
	logger.Panicf(s, i)
}
