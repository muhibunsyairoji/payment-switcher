package repositories

import (
	"context"
	"database/sql"
	"errors"
	"strconv"
	"time"

	"bitbucket.org/pergicom/payment-switcher/config"
	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/logger"
	"bitbucket.org/pergicom/payment-switcher/models"
	"bitbucket.org/pergicom/payment-switcher/utils"
	"github.com/jinzhu/gorm"
)

type PaymentScheduleRepository struct {
	*config.Clients
}

func (repo *PaymentScheduleRepository) FindPaymentScheduleByQueryRepository(ctx context.Context, param *models.PaymentSchedule, offset int, size int) ([]models.PaymentSchedule, int, error) {

	var result []models.PaymentSchedule

	orderBy := utils.SanitizeQuery(*param.OrderBy)
	if param.Descending {
		orderBy = orderBy + " desc"
	} else {
		orderBy = orderBy + " asc"
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	defer tx.Rollback()

	tx.Select("SQL_CALC_FOUND_ROWS *").Where("CONCAT_WS('',code,name) like ?", "%"+*param.Query+"%").Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Offset(offset).Limit(size).Order(orderBy, true).Find(&result)

	var rowSet models.RowSet
	tx.Raw("SELECT FOUND_ROWS() as count").Scan(&rowSet)

	for i, _ := range result {
		var paymentProductMethod models.PaymentProductMethod
		tx.Model(result[i]).Related(&paymentProductMethod)
		result[i].PaymentProductMethod = &paymentProductMethod
	}

	return result, rowSet.Count, nil
}

func (repo *PaymentScheduleRepository) FindPaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, offset int, size int) ([]models.PaymentSchedule, int, error) {
	var result []models.PaymentSchedule

	orderBy := utils.SanitizeQuery(*param.OrderBy)
	if param.Descending {
		orderBy = orderBy + " desc"
	} else {
		orderBy = orderBy + " asc"
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	defer tx.Rollback()

	if param.PaymentScheduleID != nil && *param.PaymentScheduleID > 0 {
		tx = tx.Where("payment_schedule_id = ?", *param.PaymentScheduleID)
	}

	if param.PaymentProductMethodID != nil && *param.PaymentProductMethodID > 0 {
		tx = tx.Where("payment_product_method_id = ?", *param.PaymentProductMethodID)
	}

	count := 0

	if err := tx.Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).
		Find(&result).Count(&count).Error; err != nil {
		logger.Errorf("Error FindPaymentScheduleRepository() : Find() = %v", err.Error())
		return nil, 0, err
	}

	if err := tx.Select("*").
		Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).
		Offset(offset).
		Limit(size).
		Order(orderBy, true).
		Find(&result).Error; err != nil {
		logger.Errorf("Error FindPaymentScheduleRepository() : Find() = %v", err.Error())
		return nil, 0, err
	}

	txJoin := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	for i, _ := range result {
		var paymentProductMethod models.PaymentProductMethod
		var paymentMethod models.PaymentMethod
		// var paymentGateway models.PaymentGateway

		txJoin.Model(result[i]).Related(&paymentProductMethod)
		// txJoin.Model(&paymentMethod).Related(&paymentProductMethod)
		// txJoin.Model(&paymentGateway).Related(&paymentMethod)
		//txJoin.Model(result[i]).Related(&paymentProductMethod)

		paymentProductMethod.PaymentMethod = &paymentMethod
		// paymentProductMethod.PaymentMethod.PaymentGateway = &paymentGateway
		result[i].PaymentProductMethod = &paymentProductMethod

	}

	txJoin.Rollback()

	return result, count, nil
}

func (repo *PaymentScheduleRepository) AddPaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, user *models.User) ([]models.PaymentSchedule, error) {

	now := time.Now().UTC()
	statusRecord := cons.CONST_CODE_NEW_RECORD

	param.RecordProperties = models.RecordProperties{
		CreatedAt:    &now,
		CreatedBy:    user.UserName,
		StatusRecord: &statusRecord,
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	if err := tx.Create(&param).Error; err != nil {
		tx.Rollback()
		logger.Errorf("Error AddPaymentScheduleRepository() : tx.Create() = %v", err.Error())
		return nil, err
	}

	tx.Commit()
	result := []models.PaymentSchedule{*param}

	return result, nil
}

func (repo *PaymentScheduleRepository) EditPaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, user *models.User) ([]models.PaymentSchedule, error) {

	now := time.Now().UTC()
	statusRecord := cons.CONST_CODE_UPDATED_RECORD

	param.RecordProperties = models.RecordProperties{
		UpdatedAt:    &now,
		UpdatedBy:    user.UserName,
		StatusRecord: &statusRecord,
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	payment_schedule, err := findPaymentScheduleByID(tx, *param.PaymentScheduleID)
	if err != nil {
		tx.Rollback()
		errorMsg := "Unable to edit payment_schedule : " + err.Error()
		return nil, models.NewCustomError(errorMsg)
	}

	if err := updatePaymentSchedule(tx, payment_schedule, param); err != nil {
		tx.Rollback()
		logger.Errorf("Error EditPaymentScheduleRepository() : updatePaymentSchedule() = %v", err.Error())
		return nil, err
	}

	var paymentProductMethod models.PaymentProductMethod
	tx.Model(&payment_schedule).Related(&paymentProductMethod)
	payment_schedule.PaymentProductMethod = &paymentProductMethod

	tx.Commit()
	result := []models.PaymentSchedule{*payment_schedule}

	return result, nil
}

func (repo *PaymentScheduleRepository) DeletePaymentScheduleRepository(ctx context.Context, param *models.PaymentSchedule, user *models.User) ([]models.PaymentSchedule, error) {

	now := time.Now().UTC()
	statusRecord := cons.CONST_CODE_DELETED_RECORD

	param.RecordProperties = models.RecordProperties{
		UpdatedAt:    &now,
		UpdatedBy:    user.UserName,
		StatusRecord: &statusRecord,
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	payment_schedule, err := findPaymentScheduleByID(tx, *param.PaymentScheduleID)
	if err != nil {
		tx.Rollback()
		errorMsg := "Unable to delete payment_schedule : " + err.Error()
		return nil, models.NewCustomError(errorMsg)
	}

	if err := updatePaymentSchedule(tx, payment_schedule, param); err != nil {
		tx.Rollback()
		logger.Errorf("Error DeletePaymentScheduleRepository() : updatePaymentSchedule() = %v", err.Error())
		return nil, err
	}

	tx.Commit()

	result := []models.PaymentSchedule{*payment_schedule}

	return result, nil
}

func findPaymentScheduleByID(db *gorm.DB, id int64) (*models.PaymentSchedule, error) {

	var payment_schedule models.PaymentSchedule

	if db.Where("payment_schedule_id = ?", id).Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Find(&payment_schedule).RecordNotFound() {
		return nil, errors.New("PaymentScheduleID '" + strconv.FormatInt(id, 10) + "' not found")
	}
	return &payment_schedule, nil
}

func findPaymentScheduleByCode(db *gorm.DB, code string) *models.PaymentSchedule {
	payment_schedule := &models.PaymentSchedule{}
	if db.Where("code = ?", code).Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Find(&payment_schedule).RecordNotFound() {
		return nil
	}
	return payment_schedule
}

func findPaymentScheduleByName(db *gorm.DB, name string) *models.PaymentSchedule {
	payment_schedule := &models.PaymentSchedule{}
	if db.Where("name = ?", name).Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Find(&payment_schedule).RecordNotFound() {
		return nil
	}
	return payment_schedule
}

func updatePaymentSchedule(db *gorm.DB, model *models.PaymentSchedule, param *models.PaymentSchedule) error {

	if err := db.Model(&model).Updates(&param).Error; err != nil {
		return err
	}

	return nil
}
