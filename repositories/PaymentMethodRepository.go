package repositories

import (
	"context"
	"database/sql"
	"errors"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/pergicom/payment-switcher/config"
	cons "bitbucket.org/pergicom/payment-switcher/constants"
	"bitbucket.org/pergicom/payment-switcher/logger"
	"bitbucket.org/pergicom/payment-switcher/models"
	"bitbucket.org/pergicom/payment-switcher/utils"
	"github.com/jinzhu/gorm"
)

type PaymentMethodRepository struct {
	*config.Clients
}

func (repo *PaymentMethodRepository) FindPaymentMethodByQueryRepository(ctx context.Context, param *models.PaymentMethod, offset int, size int) ([]models.PaymentMethod, int, error) {

	var result []models.PaymentMethod

	orderBy := utils.SanitizeQuery(*param.OrderBy)
	if param.Descending {
		orderBy = orderBy + " desc"
	} else {
		orderBy = orderBy + " asc"
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	defer tx.Rollback()

	tx.Select("SQL_CALC_FOUND_ROWS *").Where("CONCAT_WS('',code,name) like ?", "%"+*param.Query+"%").Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Offset(offset).Limit(size).Order(orderBy, true).Find(&result)

	var rowSet models.RowSet
	tx.Raw("SELECT FOUND_ROWS() as count").Scan(&rowSet)

	for i, _ := range result {
		var paymentGateway models.PaymentGateway
		tx.Model(result[i]).Related(&paymentGateway)
		result[i].PaymentGateway = &paymentGateway
	}

	return result, rowSet.Count, nil
}

func (repo *PaymentMethodRepository) FindPaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, offset int, size int) ([]models.PaymentMethod, int, error) {
	var result []models.PaymentMethod

	orderBy := utils.SanitizeQuery(*param.OrderBy)
	if param.Descending {
		orderBy = orderBy + " desc"
	} else {
		orderBy = orderBy + " asc"
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	defer tx.Rollback()

	if param.PaymentMethodID != nil && *param.PaymentMethodID > 0 {
		tx = tx.Where("payment_method_id = ?", *param.PaymentMethodID)
	}

	if param.PaymentGatewayID != nil && *param.PaymentGatewayID > 0 {
		tx = tx.Where("payment_gateway_id = ?", *param.PaymentGatewayID)
	}

	if param.Type != nil && strings.TrimSpace(*param.Type) != "" {
		tx = tx.Where("type = ?", *param.Type)
	}

	if param.Name != nil && strings.TrimSpace(*param.Name) != "" {
		tx = tx.Where("name LIKE ?", "%"+*param.Name+"%")
	}

	if param.Code != nil && strings.TrimSpace(*param.Code) != "" {
		tx = tx.Where("code = ?", "%"+*param.Code+"%")
	}

	count := 0

	if err := tx.Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).
		Find(&result).Count(&count).Error; err != nil {
		logger.Errorf("Error FindPaymentMethodRepository() : Find() = %v", err.Error())
		return nil, 0, err
	}

	if err := tx.Select("*").
		Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).
		Offset(offset).
		Limit(size).
		Order(orderBy, true).
		Find(&result).Error; err != nil {
		logger.Errorf("Error FindPaymentMethodRepository() : Find() = %v", err.Error())
		return nil, 0, err
	}

	txJoin := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	for i, _ := range result {
		var paymentGateway models.PaymentGateway
		txJoin.Model(result[i]).Related(&paymentGateway)
		result[i].PaymentGateway = &paymentGateway
	}

	txJoin.Rollback()

	return result, count, nil
}

func (repo *PaymentMethodRepository) AddPaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, user *models.User) ([]models.PaymentMethod, error) {

	now := time.Now().UTC()
	statusRecord := cons.CONST_CODE_NEW_RECORD

	param.RecordProperties = models.RecordProperties{
		CreatedAt:    &now,
		CreatedBy:    user.UserName,
		StatusRecord: &statusRecord,
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	var payment_method models.PaymentMethod
	if !tx.Where("code = ?", param.Code).
		Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).
		Find(&payment_method).RecordNotFound() {

		tx.Rollback()
		errorMsg := "Unable to add payment_method : duplicate payment_methodCode '" + *param.Code + "'"
		return nil, models.NewCustomError(errorMsg)

	}

	if !tx.Where("name = ?", param.Name).
		Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).
		Find(&payment_method).RecordNotFound() {

		tx.Rollback()
		errorMsg := "Unable to add payment_method : duplicate payment_methodName '" + *param.Name + "'"
		return nil, models.NewCustomError(errorMsg)
	}

	if err := tx.Create(&param).Error; err != nil {
		tx.Rollback()
		logger.Errorf("Error AddPaymentMethodRepository() : tx.Create() = %v", err.Error())
		return nil, err
	}

	tx.Commit()
	result := []models.PaymentMethod{*param}

	return result, nil
}

func (repo *PaymentMethodRepository) EditPaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, user *models.User) ([]models.PaymentMethod, error) {

	now := time.Now().UTC()
	statusRecord := cons.CONST_CODE_UPDATED_RECORD

	param.RecordProperties = models.RecordProperties{
		UpdatedAt:    &now,
		UpdatedBy:    user.UserName,
		StatusRecord: &statusRecord,
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	payment_method, err := findPaymentMethodByID(tx, *param.PaymentMethodID)
	if err != nil {
		tx.Rollback()
		errorMsg := "Unable to edit payment_method : " + err.Error()
		return nil, models.NewCustomError(errorMsg)
	}

	if param.Code != nil {
		a := findPaymentMethodByCode(tx, *param.Code)

		if a != nil && *a.PaymentMethodID != *param.PaymentMethodID {
			tx.Rollback()
			errorMsg := "Unable to edit payment_method : payment_method code already exists"
			return nil, models.NewCustomError(errorMsg)
		}
	}

	if param.Name != nil {
		a := findPaymentMethodByName(tx, *param.Name)

		if a != nil && *a.PaymentMethodID != *param.PaymentMethodID {
			tx.Rollback()
			errorMsg := "Unable to edit payment_method : payment_method name already exists"
			return nil, models.NewCustomError(errorMsg)
		}
	}

	if err := updatePaymentMethod(tx, payment_method, param); err != nil {
		tx.Rollback()
		logger.Errorf("Error EditPaymentMethodRepository() : updatePaymentMethod() = %v", err.Error())
		return nil, err
	}

	var paymentGateway models.PaymentGateway
	tx.Model(&payment_method).Related(&paymentGateway)
	payment_method.PaymentGateway = &paymentGateway

	tx.Commit()
	result := []models.PaymentMethod{*payment_method}

	return result, nil
}

func (repo *PaymentMethodRepository) DeletePaymentMethodRepository(ctx context.Context, param *models.PaymentMethod, user *models.User) ([]models.PaymentMethod, error) {

	now := time.Now().UTC()
	statusRecord := cons.CONST_CODE_DELETED_RECORD

	param.RecordProperties = models.RecordProperties{
		UpdatedAt:    &now,
		UpdatedBy:    user.UserName,
		StatusRecord: &statusRecord,
	}

	tx := repo.DB.BeginTx(ctx, &sql.TxOptions{})

	payment_method, err := findPaymentMethodByID(tx, *param.PaymentMethodID)
	if err != nil {
		tx.Rollback()
		errorMsg := "Unable to delete payment_method : " + err.Error()
		return nil, models.NewCustomError(errorMsg)
	}

	if err := updatePaymentMethod(tx, payment_method, param); err != nil {
		tx.Rollback()
		logger.Errorf("Error DeletePaymentMethodRepository() : updatePaymentMethod() = %v", err.Error())
		return nil, err
	}

	tx.Commit()

	result := []models.PaymentMethod{*payment_method}

	return result, nil
}

func findPaymentMethodByID(db *gorm.DB, id int64) (*models.PaymentMethod, error) {

	var payment_method models.PaymentMethod

	if db.Where("payment_method_id = ?", id).Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Find(&payment_method).RecordNotFound() {
		return nil, errors.New("PaymentMethodID '" + strconv.FormatInt(id, 10) + "' not found")
	}
	return &payment_method, nil
}

func findPaymentMethodByCode(db *gorm.DB, code string) *models.PaymentMethod {
	payment_method := &models.PaymentMethod{}
	if db.Where("code = ?", code).Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Find(&payment_method).RecordNotFound() {
		return nil
	}
	return payment_method
}

func findPaymentMethodByName(db *gorm.DB, name string) *models.PaymentMethod {
	payment_method := &models.PaymentMethod{}
	if db.Where("name = ?", name).Not("status_record = ?", cons.CONST_CODE_DELETED_RECORD).Find(&payment_method).RecordNotFound() {
		return nil
	}
	return payment_method
}

func updatePaymentMethod(db *gorm.DB, model *models.PaymentMethod, param *models.PaymentMethod) error {

	if err := db.Model(&model).Updates(&param).Error; err != nil {
		return err
	}

	return nil
}
