package models

import "bitbucket.org/pergicom/payment-switcher/constants"

type PaymentMethod struct {
	PaymentMethodID  *int64          `json:"paymentMethodID" gorm:"primary_key"`
	PaymentGatewayID *int64          `json:"paymentGatewayID"`
	PaymentGateway   *PaymentGateway `json:"paymentGateway" gorm:"foreignKey:payment_gateway_id"`
	Type             *string         `json:"type"`
	Code             *string         `json:"code"`
	MethodCode       *string         `json:"methodCode"`
	Name             *string         `json:"name"`
	ShortName        *string         `json:"shortName"`
	IsDefault        *bool           `json:"isDefault"`
	ISActive         *bool           `json:"isActive"`
	FilterProperties
	RecordProperties
}

func (a *PaymentMethod) TableName() string {
	return constants.TBL_PAYMENT_METHOD
}
