package models

import "bitbucket.org/pergicom/payment-switcher/constants"

type PaymentSchedule struct {
	PaymentScheduleID      *int64                `json:"paymentScheduleID" gorm:"primary_key"`
	PaymentProductMethodID *int64                `json:"paymentProductMethodID"`
	PaymentProductMethod   *PaymentProductMethod `json:"paymentProductMethod" gorm:"foreignKey:payment_product_method_id"`
	ScheduleType           *string               `json:"scheduleType"`
	ScheduleStatus         *string               `json:"scheduleStatus"`
	DateStartedAt          *string               `json:"dateStartedAt"`
	DateEndedAt            *string               `json:"dateEndedAt"`
	TimeStartedAt          *string               `json:"timeStartedAt"`
	TimeEndedAt            *string               `json:"timeEndedAt"`
	FilterProperties
	RecordProperties
}

func (a *PaymentSchedule) TableName() string {
	return constants.TBL_PAYMENT_SCHEDULE
}
