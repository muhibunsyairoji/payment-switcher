package models

import "bitbucket.org/pergicom/payment-switcher/constants"

type PaymentProductMethod struct {
	PaymentProductMethodID *int64         `json:"paymentProductMethodID" gorm:"primary_key"`
	PaymentMethodID        *int64         `json:"paymentMethodID"`
	PaymentMethod          *PaymentMethod `json:"paymentMethod" gorm:"foreignKey:payment_method_id"`
	ProductType            *string        `json:"productType"`
	ServiceCharge          *string        `json:"serviceCharge"`
	ServiceChargeType      *string        `json:"serviceChargeType"`
	MinimumPayment         *string        `json:"minimumPayment"`
	Sort                   *int           `json:"sort"`
	BankNo                 *string        `json:"bankNo"`
	BankName               *string        `json:"bankName"`
	IsActive               *bool          `json:"isActive"`
	FilterProperties
	RecordProperties
}

func (a *PaymentProductMethod) TableName() string {
	return constants.TBL_PAYMENT_PRODUCT_METHOD
}
