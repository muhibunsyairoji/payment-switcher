package models

type FilterProperties struct {
	Query      *string `json:"query,omitempty" gorm:"-"`
	Page       *int    `json:"page,omitempty" gorm:"-"`
	Size       *int    `json:"size,omitempty" gorm:"-"`
	OrderBy    *string `json:"orderBy,omitempty" gorm:"-"`
	Descending bool    `json:"descending,omitempty" gorm:"-"`
}
