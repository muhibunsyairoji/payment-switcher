package models

type User struct {
	UserID   *int64  `json:"userID"`
	UserName *string `json:"userName"`
}
