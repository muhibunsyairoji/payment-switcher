package models

type ResponseWrapper struct {
	Status      bool        `json:"status"`
	Code        int         `json:"code,omitempty"`
	Message     interface{} `json:"message,omitempty"`
	TotalData   int         `json:"totalData"`
	Data        interface{} `json:"data,omitempty"`
	ElapsedTime interface{} `json:"elapsedTime,omitempty"`
	HasMorePage *bool       `json:"hasMorePage,omitempty"`
}
