package models

import "bitbucket.org/pergicom/payment-switcher/constants"

type PaymentGateway struct {
	PaymentGatewayID *int64  `json:"paymentGatewayID" gorm:"primary_key"`
	Code             *string `json:"code"`
	Name             *string `json:"name"`
	FilterProperties
	RecordProperties
}

func (a *PaymentGateway) TableName() string {
	return constants.TBL_PAYMENT_GATEWAY
}
