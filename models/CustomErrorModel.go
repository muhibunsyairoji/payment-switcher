package models

type CustomError struct {
	message string
}

func NewCustomError(message string) *CustomError {
	return &CustomError{
		message: message,
	}
}

func (e *CustomError) Error() string {
	return e.message
}