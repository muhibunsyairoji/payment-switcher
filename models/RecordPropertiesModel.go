package models

import "time"

type RecordProperties struct {
	CreatedAt    *time.Time `json:"createdAT" time_format:"2006-01-02T15:04:05" sql:"type:datetime" time_location:"UTC"`
	CreatedBy    *string    `json:"createdBy" sql:"type:varchar(50)"`
	UpdatedAt    *time.Time `json:"updatedAt" time_format:"2006-01-02T15:04:05" sql:"type:datetime" time_location:"UTC"`
	UpdatedBy    *string    `json:"updatedBy" sql:"type:varchar(50);"`
	StatusRecord *string    `json:"statusRecord"`
}

type RowSet struct {
	Count int
}
